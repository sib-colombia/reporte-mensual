Este archivo contiene una breve descripción de como realizar el reporte mensual de publicación utilizando el script reporteMensualPublicacion.py, la recomendación es correr este script el primer día del mes, del paso 1 al 3. Los otros pasos se pueden hacer en luego pero ya quedan los datos con el corte adecuado.

# Pasos para correr el script:

1) Abrir el script en Spyder y modificar la línea 25 la variable rutaAbsolutoArchivos con la ruta donde van a estar los insumos y donde se va a exportar el resultado.
2) Al interior de la carpeta que definió en el paso anterior, suba los siguientes archivos manteniendo el nombre:

*OrganizacionesRegistradas_AAAAMMDD*: El archivo de [organiazaciones registradas](https://docs.google.com/spreadsheets/d/1DbPzsn48Af_tAbTH0unp0DgIlYyvLQBBGkX-HW_gk8Y/edit#gid=2059226080) actualizado, solamente suba la hoja Socios publicadores (consolidado) y elimine las dos primeras filas con los encabezados.
*datasetCO_Anterior_AAAAMMDD*: Los datos del reporte mensual de publicación del mes inmediatamente anterior, lo puede descargar en [este repositorio](https://gitlab.com/sib-colombia/reporte-mensual/-/tree/main/)

3) Ponga a correr todo el script completamente, no es necesario correrlo por pasos. Este proceso puede tardar 20-30 minutos.

# Pasos para realizar el reporte mensual:
4) Vaya a la carpeta que definió y extraiga el archivo de resultados, si corrió el scrip el primer día del mes el nombre del archivo debe ser datasetCO_AAAA-MM-DD.txt de acuerdo a la fecha para la que esté haciendo el reporte. Si lo corrió en una fecha diferente, el nombre del archivo tendrá la fecha del día anterior al que lo ejecutó.

5) Cargue el archivo en OpenRefine y realice los siguientes procesos:
- Revisar la columna de actualizaciones, para asegurarse que esté correctamente documentada, adeicionalmente documente las actualizaciones de listas.
Para documentar las listas de especies actualizadas, utilice el trello de acompañamientos con un filtro para las listas actualizadas. para las listas d elos institutos entre a cada conjunto de lista que se actualizo en el mes y revise la descripción y el número de especies. Marque las que hayan teneido mas registros.
Para los eventos de muestreo sin registros, entre a a cada uno y revise si aumentó el numero de evento, si es así pongao como actulaización, sino no es necesario.
- Si realizo el reporte posterior al primer día del mes, filtre los datos por created y elimine los recursos que no corresponden al mes que está reportando


5.1) **Los llamados al API de CITAS y la url del IPT no sirven en el script de python, toca hacerlos en OpenRefine**

6) Para continuar con el proceso del reporte mensual, siga el procedimiento en el archivo a partir de [Reporte mensual - spreadsheet datastudio](https://docs.google.com/document/d/1CLz-BB5RDktcbEkTkP19PcsuhRyGOEprKR2stbQn9o8/edit#heading=h.hl6go1ovc7z8)

7) Para el final de año, saque el número de actualizaciones y nuevos conjuntos de datos completos del año al compararlo con el reporte del año pasado + cree la columna NumeroCitasDataset con la info del número de citas por conjunto de datos en OpenRefine:"https://api.gbif.org/v1/literature/search?gbifDatasetKey="+value