#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Rutinas del SiB Colombia para la generacion de reportes mensuales
# Nombre: Gneracion de reporte mensual de publicacion a través del SiB Colombia
# Languaje: python
# Creado:2022-05-04
# Ultima Actualizacion:2023-02-01
# Autores: Esteban Marentes, Nerieth Leuro, Ricardo Ortiz, Camila Plata
# Proposito y uso: Script para sacar las cifras de publicacion mensuales de un paí­s en GBIF
#__________________________________________________________

##----------------------------------------------------------------------Preparar el entorno ---------------------------------------------------------------------------

import pandas as pd
import requests
import os 
import json
import time
from pandas.io.json import json_normalize
from datetime import date
from datetime import timedelta
import numpy as np

#### Variables de trabajo
os.chdir("/Users/estebanmarentes/Desktop/EstebanMH/Otros trabajos AC/ReporteMensual")
codigoPaisGbif = '7e865cba-7c46-417b-ade5-97f2cf5b7be0'

## Variables de la fecha, se crean automáticamente según el día
todayPlusOne = date.today() # Llamar la fecha del día en el que estamos para que el resultado final salga con el nombre correcto
today = todayPlusOne - timedelta(days=1) # Restar un día porque hago el llamado el primero del mes y el nombre del archivo es del último del mes
nombre='datasetCO_' + str(today)
corte=str(today)
##----------------------------------------------------------------------Importar archivos que se van a utilizar---------------------------------------------------------------------------
'''
Archivo de organizaciones en Excel. 
Antes de descargar el archivo, eliminar las dos filas del comienzo
'''
##Cargar el archivo de organizaciones
OrganizacionesRegistradas_AAAAMMDD = pd.read_excel('OrganizacionesRegistradas_AAAAMMDD.xlsx')

## Eliminar las columnas inncesarias para dejar únicamente las que se necesitan: Nivel 1, GBIF ID, Logo Github, Nombre corto y URL Portal de Datos SiB
OrganizacionesRegistradas_AAAAMMDD = OrganizacionesRegistradas_AAAAMMDD[["GBIF ID", "Nivel 1", "Logo Github","Nombre corto", "URL registro GBIF" ]]

## Renombrar las columnas 
OrganizacionesRegistradas_AAAAMMDD.rename(columns = {'GBIF ID': 'publishingOrganizationKey', 'Nivel 1': 'typeOrg', 'Logo Github': 'Logo','Nombre corto': 'NombreCorto_Org', 'URL registro GBIF': 'URLSocio'}, inplace = True)


## Cargar el archivo del reporte del mes anterior
datasetCO_Anterior_AAAAMMDD = pd.read_csv('datasetCO_Anterior_AAAAMMDD.txt', encoding = "utf8", sep="\t")


##------------1. Llamada de datos iniciales en el API de GBIF para todos los conjuntos de datos del nodo Colombia a un dataFrame Pandas---------##
'''
La API de GBIF solo permite hacer un llamado de maximo 1000 registros. Por lo cual, se debe crear la cantidad de variables suficientes para
cubrir la cantidad de datos llamados.
'''

## Llamada al API desde el dato 1 al 1000
response1000_JSON = pd.read_json("http://api.gbif.org/v1/node/" +codigoPaisGbif+ "/dataset?limit=1000") 

##Llamada al API desde el dato 1000 al 2000, si hay más de 2000 recursos publicados, toca hacer uno llamado extra
response2000_JSON = pd.read_json("http://api.gbif.org/v1/node/" +codigoPaisGbif+ "/dataset?limit=1000&offset=1000") 

## Llamada al API desde el dato 2000 al 3000
response3000_JSON = pd.read_json("http://api.gbif.org/v1/node/" +codigoPaisGbif+ "/dataset?limit=1000&offset=2000") 

##-------------2. Convertir el archivo JSON a tablas-------------------------------------------------------------------------------------------##
'''
Convertir el archivo JSON original en una tabla utilizando pandas.normalize para extraer los datos que estaban anidados en el JSON
'''

response1000 = pd.json_normalize(response1000_JSON['results']) 
response2000 = pd.json_normalize(response2000_JSON['results']) 
response3000 = pd.json_normalize(response3000_JSON['results']) 

## Concatenar el resultado de los dos llamados del API
resultadosAPI = pd.concat([response1000, response2000, response3000])

##------------3. Seleccionar y ajustar las columnas necesarias para el reporte----------------------------------------------------------------##
'''
Ejecutar pasos para extraer las columnas necesarias para el reporte, renombre las columnas y crear los campos adicionales necesarios para 
el reporte en cada archivo
'''
## Escoger las columnas que se van ausar
resultadosAPI = resultadosAPI[["key", "doi", "type","title", "created", "modified","version", "publishingOrganizationKey"]]

## Separar la informacion de la columna created en año, mes, dí­a
##Separa la fecha de la hora usando T como separador
resultadosAPI[['created1','created2']] = resultadosAPI.created.str.split("T",expand=True,) 
## Elimina la columna que quedo con la hora
del resultadosAPI['created2']

## Separa la fecha en año, mes dí­a y crea las columnas
resultadosAPI[['year','month','day']] = resultadosAPI.created1.str.split("-",expand=True,) 
## Elimina la columna original
del resultadosAPI['created']

## cambia nombre a created de la columna corregida
resultadosAPI.rename(columns = {'created1':'created'}, inplace = True) 


## Separar la informacion de la columna modified en año, mes, dí­a
## Separa la fecha de la hora usando T como separador
resultadosAPI[['modified1','modified2']] = resultadosAPI.modified.str.split("T",expand=True,) 
## Elimina la columna que quedo con la hora
del resultadosAPI['modified2']

## Separa la fecha en año, mes dí­a y crea las columnas
resultadosAPI[['year-mod','month-mod','day-mod']] = resultadosAPI.modified1.str.split("-",expand=True,)
## Elimina la columna original
del resultadosAPI['modified']

## cambia nombre a created de la columna corregida
resultadosAPI.rename(columns = {'modified1':'modified'}, inplace = True) 

## Modificar la columna doi para agregar el prefijo y cambiar el nombre
resultadosAPI[['doi']] = 'http://doi.org/' + resultadosAPI[['doi']].astype(str)
resultadosAPI.rename(columns = {'doi':'DOI_URL'}, inplace = True)


## Modificar el nombre de los type y pasarlo a español, en este paso toca crear un nuevo DataFrame para guardar el resultado del remplazo
resultadosAPI['type']=resultadosAPI['type'].replace('OCCURRENCE', 'Registros biologicos').replace('CHECKLIST', 'Listas de especies').replace('METADATA', 'Metadatos').replace('SAMPLING_EVENT', 'Eventos de muestreo')

## Eliminar la fila del recurso del CIAT asociada a "A global database for the distributions of crop wild relatives"
resultadosAPI=resultadosAPI[(resultadosAPI['key']!='07044577-bd82-4089-9f3a-f4a9d2170b2e') & ( ['title'] != 'A global database for the distributions of crop wild relatives')]


## Llamado al API de GBIF para extraer la informacion del nombre de la organizacion y ponerla en una nueva columna organization

## Seleccionar el identificador de cada conjunto de datos y el identificación de la organización
dfo=resultadosAPI[["key","publishingOrganizationKey"]].drop_duplicates('publishingOrganizationKey',inplace=False) 


## Hacer el llabado de la API GBIF para publishingOrganizationKey
def call_gbif_title(row):
    try:
        url = "http://api.gbif.org/v1/organization/"+ str(row['publishingOrganizationKey'])      
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.005)
        return response_json
    except Exception as e:
        raise e

##Crear la columna 'API_response' donde se almacena el resultado del llamado de la API para cada registro
dfo['API_response'] = dfo.apply(call_gbif_title,axis=1)

## Convertir los resultados del llamado en una tabla
norm_ok = json_normalize(dfo['API_response'])

##Seleccionar las columnas necesarias del publicador y renombrarlas
ok = norm_ok[['key','title']]
ok.rename(columns={'title': 'organization', 'key': 'publishingOrganizationKey'}, inplace=True)

#Unir la tabla general con la que contiene la información de los publicadores
resultadosAPI=pd.merge(resultadosAPI,ok,how='left',on='publishingOrganizationKey')


## Llamado al API de GBIF API para extraer la informacion endpoints

## subset de los datos con las columnas a llamar
api_dataset=resultadosAPI[["key"]] 
dfdk=api_dataset.drop_duplicates('key',inplace=False)

#Define the gbif API call for publishingOrganizationKey
def call_gbif_endpoints(row):
    try:
        url = "http://api.gbif.org/v1/dataset/"+ str(row['key'])      
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.005)
        return response_json
    except Exception as e:
        raise e

dfdk['API_response'] = dfdk.apply(call_gbif_endpoints,axis=1)

norm_dk = json_normalize(dfdk['API_response'])
dk = norm_dk[['key','endpoints']]

# Paso intermedio para guardar toda la informacion de dk en un nuevo dataframe
dfEndpoints = dk
# Separar el llamado de los varios diccionarios a 4 columnas, aquí­ está la magia https://stackoverflow.com/questions/64037243/pythonhow-to-split-column-into-multiple-columns-in-a-dataframe-and-with-dynamic
d = [pd.DataFrame(dfEndpoints[col].tolist()).add_prefix(col) for col in dfEndpoints.columns]
dfEndpoints = pd.concat(d, axis=1)


# Extraer el json dentro del dataframe
dfEndpoints[['keyInterna','type','url','createdBy','modifiedBy','created','modified','machineTags']] = json_normalize(dfEndpoints['endpoints0'])
# Cambiar el nombre de la columna llave-key
dfEndpoints.rename(columns={'key0': 'key'}, inplace=True)


# Separar la URL para extraer el IPT y el nombrecorto

dfEndpoints[['PrevioIPT','nombrecorto']] = dfEndpoints.url.str.split("=",expand=True,) # Separa la URL usando = para dejar solo el nombre corto
dfEndpoints[['IPTtemporal','desctarteIPT']] = dfEndpoints.PrevioIPT.str.split("/archive",expand=True,) # Separa la URL del IPT quitando el /archive
dfEndpoints['IPTtemporal']=dfEndpoints.IPTtemporal.str.replace('//', '-') # Reemplazar solamente una parte del string para quitar el // y poder separarlos en el siguiente paso
dfEndpoints[['IPTdescartado','IPT','EMLTemporal']] = dfEndpoints.IPTtemporal.str.split("/",expand=True,)

# Dejar solamente el resultado deseado
dfEndpoints = dfEndpoints[["key", "nombrecorto", "IPT"]]

#merge resultado de nombre corto e IPT

resultadosAPI=pd.merge(resultadosAPI,dfEndpoints,how='left',on='key')

#### 5 Obtener número de registros por conjunto de datos

### 5.1 a 5.3

## URL para el llamado a los conjuntos de datos
ApiNumeroRegistros = 'http://api.gbif.org/v1/occurrence/search?publishingCountry=CO&limit=0&facet=datasetKey&facetLimit=3000'
# download the data https://stackoverflow.com/questions/50531308/valueerror-arrays-must-all-be-same-length

# Realizar el llamado a la URL y guardarlos
llamadoApiNumeroRegistros = requests.get(url=ApiNumeroRegistros)
# Convertir el llamado a un JSON
JSON_NumeroRegistros = json.loads(llamadoApiNumeroRegistros.content)
# Extrare del JSON la parte correspondiente al diccionario de facets
JSON_Normalizado = pd.DataFrame.from_dict(JSON_NumeroRegistros['facets'][0])
# Normalizar el JSON y dejar el archivo organizado
numberOfRecords_AAAAMMDD = pd.json_normalize(JSON_Normalizado['counts'])

### 5.4 Modificar el nombre de las columnas

numberOfRecords_AAAAMMDD.rename(columns = {'name':'key', 'count':'numberOfRecords'}, inplace = True)

### 5.5 Extraer el número de registros por dataset y anexarlo al recurso a exportar

resultadosAPI=pd.merge(resultadosAPI,numberOfRecords_AAAAMMDD,how='left',on='key')


#### 6 Asignar el tipo de organizacion publicadora a cada conjunto de datos
# Antes de realizar este paso asegurese de haber cargado los datos al comienzo del archivo y haber seleccionado las columnas a importar

resultadosAPI=pd.merge(resultadosAPI,OrganizacionesRegistradas_AAAAMMDD,how='left',on='publishingOrganizationKey')# Cruzar la informacion desde el archivo de organizaciones


#### 7 Rastrear los Recursos_ACtualizados durante el mes

datasetCO_Anterior_AAAAMMDD = datasetCO_Anterior_AAAAMMDD[['key','numberOfRecords']] #Quedarse solo con la columna del número de registros
datasetCO_Anterior_AAAAMMDD.rename(columns={'numberOfRecords': 'Indexacion_MesAnterior'}, inplace=True) # modificar el nombre de la columna numberOfRecors
#Unir los datos del mes anterior con el dataset general
resultadosAPI=pd.merge(resultadosAPI,datasetCO_Anterior_AAAAMMDD,how='left',on='key')


# Evaluar los datos del presente mes con los datos del mes anterior
resultadosAPI.loc[resultadosAPI['numberOfRecords'] == resultadosAPI['Indexacion_MesAnterior'], 'Cambios_Datos'] = '1' # Crear la nueva columna evaluando si son iguales y poniendo 0
resultadosAPI.loc[resultadosAPI['numberOfRecords'] != resultadosAPI['Indexacion_MesAnterior'], 'Cambios_Datos'] = '0' # Modificar la columna para poner 0 sino son iguales

# Poner como 1 el valor de la actualizacion para las listas que al no tener número de registros dan 0
resultadosAPI.loc[resultadosAPI['type'] == 'Listas de especies', 'Cambios_Datos'] = '1'

# Crear la nueva columna indicando que son actualizaciones
#resultadosAPI.loc[(resultadosAPI['Cambios_Datos'] == '0') & (resultadosAPI['Indexacion_MesAnterior']==np.nan), 'Actualizaciones'] = 'Nuevo'
#resultadosAPI.loc[(resultadosAPI['Cambios_Datos'] == '0') & (resultadosAPI['Indexacion_MesAnterior']!=np.nan), 'Actualizaciones'] = 'Actualizacion'
resultadosAPI.loc[resultadosAPI['Cambios_Datos'] == '0', 'Actualizaciones'] = 'Actualizacion'

# Crear la nueva columna Cambios_Datos

resultadosAPI['Incremento_Actualizacion'] = resultadosAPI['numberOfRecords'] - resultadosAPI['Indexacion_MesAnterior']


#### 8 llamar el número de citas

api_cites=resultadosAPI[["publishingOrganizationKey"]] # subset de los datos por la llave de organizacion
apiCallOrg1=api_cites.drop_duplicates('publishingOrganizationKey',inplace=False) # quitar los duplicados para que se más rápido el llamado
#Define the gbif API call for publishingOrganizationKey
def call_gbif_cites(row):
    try:
        url = 'https://api.gbif.org/v1/literature/search?publishingOrganizationKey=' + str(row['publishingOrganizationKey'])      
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.005)
        return response_json
    except Exception as e:
        raise e

apiCallOrg1['API_response'] = apiCallOrg1.apply(call_gbif_cites,axis=1) # Llamada del API al interior del datafram
apiCallOrg1[['offset','limit','endOfRecords','count','results','final']] = json_normalize(apiCallOrg1['API_response']) # Normalizar la llamada y ponerla en las 6 columnas apropiadas

# Separar el llamado de los varios diccionarios a 8 columnas

apiCallOrg1 = apiCallOrg1[['publishingOrganizationKey','API_response']] 
apiCallOrg2 = [pd.DataFrame(apiCallOrg1[col].tolist()).add_prefix(col) for col in apiCallOrg1.columns]
apiCallOrg3 = pd.concat(apiCallOrg2, axis=1)
apiCallOrg3 = apiCallOrg3[['publishingOrganizationKey0','API_responsecount']]
apiCallOrg3.rename(columns={'publishingOrganizationKey0': 'publishingOrganizationKey'}, inplace=True)


apiCallOrg3['URL_Citaciones'] = 'https://www.gbif.org/resource/search?contentType=literature&publishingOrganizationKey='+apiCallOrg3['publishingOrganizationKey'] # Crear la columna con el enlace
apiCallOrg = apiCallOrg3[['publishingOrganizationKey','API_responsecount','URL_Citaciones']] # Modificar el dataset para conservar solo las columnas que se van a unir
apiCallOrg.rename(columns={'API_responsecount': 'total_citesOrg'}, inplace=True) # modificar el nombre de la columna count

#Unir la llamada del API Cites con el dataset general
resultadosAPI=pd.merge(resultadosAPI,apiCallOrg,how='left',on='publishingOrganizationKey')


#### 8.5 Eliminar los datos del mes si se hace el llamado posteriormente (Yo creo que no hace falta si lo llamamos el dí­a que es )
#todayPlusOne = date.today() # Llamar la fecha del dí­a en el que estamos para que el resultado final salga con el nombre correcto
#today = todayPlusOne - timedelta(days=1) # Restar un dí­a porque hago el llamado el primero del mes y el nombre del archivo es del último del mes

#### 9 Exportar el resultado final completo
# Organizacion del resultado del archivo final SIN REALIZAR DE AQUí PARA ABAJO
resultadosFinales = resultadosAPI[[ "key", "total_citesOrg", "URL_Citaciones", "Actualizaciones", "Incremento_Actualizacion", "Cambios_Datos", "Indexacion_MesAnterior", "IPT", "numberOfRecords", "nombrecorto", "type", "organization", "title", "DOI_URL", "created", "year", "month", "day", "modified", "year-mod", "month-mod", "day-mod", "version", "NombreCorto_Org", "Logo", "typeOrg", "publishingOrganizationKey", "URLSocio"]]
resultadosFinales = resultadosFinales.drop_duplicates('key')   
resultadosFinales=resultadosFinales.query('created<=@corte or modified<=@corte')


# Exportar resultado hasta el momento como datasetCO_AAAAMMDD
#(MM: mes del reporte, DD:  el último dí­a del mes de reporte,  así­ el proceso se esté realizando en dí­as posteriores)

#x='datasetCO_' + str(2022-09-01)+'.txt' # Darle el nombre al archivo
#x='datasetCO_prueba.txt' 
resultadosFinales.to_csv( nombre+'.txt', sep="\t", encoding = "utf8")
#y='datasetCO_' + str(2022-09-01)+'.xlsx'
#y='datasetCO_prueba.xlsx'
resultadosFinales.to_excel(nombre+'.xlsx', sheet_name='cifrasEntidades', index=False )
#resultadosFinales.to_excel( y)

#### 10 Exportar los datos parciales que toca pegar en el dataStudio


# Tipo de publicador + Logo

resultadosTipoPublicador = resultadosFinales[[ "organization","typeOrg", "URLSocio", "publishingOrganizationKey"]]

resultadosTipoPublicador.to_excel(nombre+'_TipoPublicador.xlsx', sheet_name='cifrasEntidades', index=False )
#x='datasetCO_pruebaTipoPublicador.xlsx'
#resultadosTipoPublicador.to_excel(x)


# Todos los publicadores/Tipo de publicador / N. Registros / Total-Tipo

resultadosNumeroRegistros = resultadosFinales[[ "organization","typeOrg", "numberOfRecords", "type"]]
resultadosNumeroRegistros.sort_values('organization')

#x='datasetCO_pruebaNumeroRegistros.xlsx'
#resultadosNumeroRegistros.to_excel(x)
resultadosNumeroRegistros.to_excel(nombre+'_NumeroRegistros.xlsx', sheet_name='cifrasEntidades', index=False )

# Cifras uso de datos (citaciones)
resultadosCitaciones = resultadosFinales[[ "organization","typeOrg", "total_citesOrg", "URL_Citaciones"]]


resultadosCitaciones=resultadosCitaciones.drop_duplicates('organization',inplace=False)
#x='datasetCO_pruebaCitaciones.xlsx'
#resultadosCitaciones.to_excel(x)
resultadosCitaciones.to_excel(nombre+'_Citaciones.xlsx', sheet_name='cifrasEntidades', index=False )



#### Ha finalizado el proceso del reporte mensual en este script de python
#### vaya al documento Procedimiento Reporte Mensual(https://docs.google.com/document/d/1CLz-BB5RDktcbEkTkP19PcsuhRyGOEprKR2stbQn9o8/edit#heading=h.hl6go1ovc7z8) para continuar con el proceso


## Cosas que faltan por hacer: 
# Mejorar la columna de actualizaciones, para que ponga "Nuevo" y "Actualización" según corresponda
# Crear la ejecución automática del script en el PC #https://pythondiario.com/2021/02/como-ejecutar-scripts-python-automaticamente-todos-los-dias-en-windows.html

# Mejorar la exportación de los datos de la segunda parte del reporte, para que exporte solo lo que este realmente

# Hay unos datos que toca sacer en excel (suma total registros publicados, registros publicados por socio en este periodo, número de tipo de acompañamiento por socio en este periodo)
# Crear diferentes filas del escript para esto para que salga todo desde este script y no toque abrir el excel para casi nada

# Yo veo complicado crear la hoja de excel del mes por la estructura que tiene desde este script, pero se puede evaluar en un futuro
